<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <?php foreach($errors as $err): ?>
              <strong><?= $err[0]; ?></strong><br>
        <?php endforeach;?>
    </div>
<?php endif;?>

<?php if(Yii::$app->session->hasFlash('Success')): ?>
    <div class="alert alert-success">
        <strong><?= Yii::$app->session->getFlash('Success');?></strong>
    </div>
<?php endif;?>

<form id="game-order-form" action="" method="post">
    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
    <div class="clearfix margin-bottom-10">
        <div class="form-group">
            <label class="control-label" for="email" id="email-input-label">E-mail</label>
            <input type="text" id="email" class="form-control check-user-validated" name="email" autocomplete="off" required/>

            <label class="control-label password-input-label" for="password">Password</label>
            <input type="password" id="password" class="form-control password-field" name="password" autocomplete="off" required/>

            <div class="form-group">
                <label for="game-select">Select Game:</label>
                <select class="form-control" name="game-select" id="game-select">
                    <option value="0">Select</option>
                    <option value="1">Call of Duty</option>
                    <option value="2">Diablo 3</option>
                    <option value="3">Need for Speed</option>
                    <option value="4">Counter-Strike</option>
                    <option value="5">Metro</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <button type="submit"  data-style="zoom-out" class="btn btn-success btn-modal btn-full ladda-button">SUBMIT</button>
        </div>
        <div class="col-sm-6">
            <button type="button" class="btn btn-dicard btn-modal btn-full" id="modal-login-cancel-button" data-dismiss="modal">Cancel</button>
        </div>
    </div>
</form>